package br.grupointegrado.bhpachulski.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnOlaMundo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        btnOlaMundo = (Button) findViewById(R.id.btnOlaMundo);

        btnOlaMundo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Toast.makeText(view.getContext(), "Olá mundo", Toast.LENGTH_SHORT).show();

            }

        });

    }

}