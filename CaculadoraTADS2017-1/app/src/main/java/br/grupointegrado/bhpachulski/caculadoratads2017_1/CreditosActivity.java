package br.grupointegrado.bhpachulski.caculadoratads2017_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class CreditosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creditos);

        Intent intent = getIntent();

        String stringExtra = intent.getStringExtra("teste01");
        Integer intExtra = intent.getIntExtra("teste03", 0);
        double doubleExtra = intent.getDoubleExtra("teste02", 0.0);

        Log.d("TesteDaIntent", stringExtra);
        Log.d("TesteDaIntent", intExtra.toString());
        Log.d("TesteDaIntent", doubleExtra + "");

        Oi objeto = (Oi) intent.getSerializableExtra("objetoTeste");

        Log.e("TesteDaIntentObjeto",objeto.oi);

    }
}
