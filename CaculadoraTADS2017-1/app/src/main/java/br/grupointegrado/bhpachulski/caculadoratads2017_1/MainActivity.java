package br.grupointegrado.bhpachulski.caculadoratads2017_1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edtNumero1;
    private EditText edtNumero2;

    private TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNumero1 = (EditText) findViewById(R.id.edtNumero1);
        edtNumero2 = (EditText) findViewById(R.id.edtNumero2);

        tvResultado = (TextView) findViewById(R.id.tvResultado);

    }

    public void soma (View v) {
        mostraResultado(getValor(edtNumero1) + getValor(edtNumero2));
    }

    public void subtracao (View v) {
        mostraResultado(getValor(edtNumero1) - getValor(edtNumero2));
    }

    public void multiplicacao (View v) {
        mostraResultado(getValor(edtNumero1) * getValor(edtNumero2));
    }

    public void divisao (View v) {
        mostraResultado(getValor(edtNumero1) / getValor(edtNumero2));
    }

    public void limpar (View v) {
        LinearLayout ln = (LinearLayout) findViewById(R.id.activity_main);

        for (int i=1; i < ln.getChildCount(); i++) {

            if (ln.getChildAt(i) instanceof EditText)
                ((EditText) ln.getChildAt(i)).setText("");

        }
    }

    public void mostraResultado (Double resultado) {

        if (!resultado.isInfinite() && !resultado.isNaN()) {

            tvResultado.setText(resultado.toString());
            Toast.makeText(this, resultado.toString(), Toast.LENGTH_SHORT).show();

        }

    }

    public Double getValor (EditText edt) {

        try {

            return Double.parseDouble(edt.getText().toString());

        } catch (NumberFormatException e) {
            Toast.makeText(this, "O campos de valor são obrigatórios.", Toast.LENGTH_LONG).show();

            edt.setText("0");
            return 0.0;
        }

    }

    public void mostraCreditos (View v) {

        Intent intentCreditoActivity = new Intent(this, CreditosActivity.class);

        intentCreditoActivity.putExtra("teste01", "Teste do Bruno");
        intentCreditoActivity.putExtra("teste02", 5.5);
        intentCreditoActivity.putExtra("teste03", 100);

        Oi oi = new Oi();

        intentCreditoActivity.putExtra("objetoTeste", oi);

        startActivity(intentCreditoActivity);

    }

}
