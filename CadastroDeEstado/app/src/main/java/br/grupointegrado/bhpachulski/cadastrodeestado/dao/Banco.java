package br.grupointegrado.bhpachulski.cadastrodeestado.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.grupointegrado.bhpachulski.cadastrodeestado.R;
import br.grupointegrado.bhpachulski.cadastrodeestado.model.Estado;

/**
 * Created by bhpachulski on 04/04/17.
 */

public class Banco extends SQLiteOpenHelper {

    private static final String DB_NAME = "CadastroDeEstado";
    private static final int DB_VERSION = 1;

    private Context context;

    public Banco(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(context.getResources()
                .getString(R.string.createEstado));
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {}

    public long salvaEstado (Estado e) {
        ContentValues cv = new ContentValues();
        cv.put("nome", e.getNome());
        cv.put("sigla", e.getSigla());

        return getWritableDatabase().insert("Estado", null, cv);
    }

    public List<Estado> listEstado () {

        List<Estado> estados = new ArrayList<>();

        Cursor c = getReadableDatabase().rawQuery(context.getResources().
                getString(R.string.listEstados), null);

        while (c.moveToNext()) {

            Estado e = new Estado();
            e.setId(c.getInt(c.getColumnIndex("id")));
            e.setNome(c.getString(c.getColumnIndex("nome")));
            e.setSigla(c.getString(c.getColumnIndex("sigla")));

            estados.add(e);

        };

        c.close();

        return estados;
    }

    public void updateEstado (Estado e) {
        ContentValues cv = new ContentValues();
        cv.put("nome", e.getNome());
        cv.put("sigla", e.getSigla());

        getWritableDatabase().update("Estado", cv, "id=?", new String[]{e.getId().toString()});
    }

    public void deleteEstado (Estado e) {
        getWritableDatabase().delete("Estado", "id=?", new String[]{e.getId().toString()});
    }

}