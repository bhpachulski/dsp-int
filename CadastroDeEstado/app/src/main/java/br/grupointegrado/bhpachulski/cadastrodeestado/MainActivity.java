package br.grupointegrado.bhpachulski.cadastrodeestado;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import br.grupointegrado.bhpachulski.cadastrodeestado.dao.Banco;
import br.grupointegrado.bhpachulski.cadastrodeestado.model.Estado;
import br.grupointegrado.bhpachulski.cadastrodeestado.ui.EstadoArrayAdapter;

public class MainActivity extends AppCompatActivity {

    private ListView lvEstados;
    private Banco banco;

    private List<Estado> listEstado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        banco = new Banco(this);

        lvEstados = (ListView) findViewById(R.id.lvEstados);

        lvEstados.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

                contextMenu.setHeaderTitle("Estado");
                getMenuInflater().inflate(R.menu.menu_update, contextMenu);

            }
        });

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo pListMenu = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {

            case R.id.mnAlterar:

                    alterar(listEstado.get(pListMenu.position));

                break;

            case R.id.mnDeletar:

                    confirmExclusao (listEstado.get(pListMenu.position));

                break;

        }

        return super.onContextItemSelected(item);
    }

    public void confirmExclusao (final Estado e) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                banco.deleteEstado(e);
                MainActivity.this.onResume();

            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {}
        });


        builder.setMessage("Você deseja realmente deletar " + e.getNome() + " ?");
        AlertDialog dialog = builder.create();


        dialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String s = "";

        switch (item.getItemId()) {
            case R.id.mnTeste:

                 s = "Botão teste";

                break;

            case R.id.action_settings:

                s = "Botão teste";

                break;
        }

        Toast.makeText(this, s, Toast.LENGTH_LONG).show();

        return super.onOptionsItemSelected(item);
    }

    public void alterar (Estado e) {
        Intent i = new Intent(this, EstadoActivity.class);
        i.putExtra("estado", e);

        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();

        listEstado = banco.listEstado();

        EstadoArrayAdapter aaEstado = new EstadoArrayAdapter(this, listEstado);

        lvEstados.setAdapter(aaEstado);
    }

    public void toCadastro (View v) {
        startActivity(new Intent(this, EstadoActivity.class));
    }
}