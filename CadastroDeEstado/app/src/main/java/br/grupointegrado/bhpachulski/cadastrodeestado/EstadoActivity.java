package br.grupointegrado.bhpachulski.cadastrodeestado;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.grupointegrado.bhpachulski.cadastrodeestado.dao.Banco;
import br.grupointegrado.bhpachulski.cadastrodeestado.model.Estado;

public class EstadoActivity extends AppCompatActivity {

    private EditText edtId;
    private EditText edtNome;
    private EditText edtSigla;

    private Button btnSalvar;
    private Button btnDeletar;

    boolean alteracao = false;

    private Banco banco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado);

        edtId = (EditText) findViewById(R.id.edtId);
        edtNome = (EditText) findViewById(R.id.edtNome);
        edtSigla = (EditText) findViewById(R.id.edtSigla);

        banco = new Banco(this);

        try {

            retornaDados((Estado) getIntent().getSerializableExtra("estado"));
            btnSalvar.setText("Alterar");
            alteracao = true;
            btnDeletar.setVisibility(View.VISIBLE);

        } catch (NullPointerException e) {
            Log.v("ESTADO", "Não veio estado para alteração");
        }

    }

    public Estado getEstado () {
        Estado e = new Estado();

        try {

            e.setId(Integer.parseInt(edtId.getText().toString()));

        } catch (NumberFormatException ex) {
            Log.v("MSG", "Não é uma alteração");
        }

        e.setNome(edtNome.getText().toString());
        e.setSigla(edtSigla.getText().toString());

        return e;
    }

    public void retornaDados (Estado e) {
        edtId.setText(e.getId().toString());
        edtNome.setText(e.getNome());
        edtSigla.setText(e.getSigla());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Estado e = getEstado();

        switch (item.getItemId()) {
            case R.id.mnAlterar:
                    banco.updateEstado(e);
                break;

            case R.id.mnSalvar:
                    banco.salvaEstado(e);
                break;

            case R.id.mnDeletar:
                    banco.deleteEstado(e);
                break;

            default:
                    this.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (alteracao) {
            getMenuInflater().inflate(R.menu.menu_update, menu);

            menu.getItem(2).setVisible(true);

        } else {
            getMenuInflater().inflate(R.menu.menu_cad, menu);
        }

        return super.onCreateOptionsMenu(menu);
    }
}
