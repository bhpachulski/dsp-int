package br.grupointegrado.bhpachulski.cadastrodeestado.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.grupointegrado.bhpachulski.cadastrodeestado.R;
import br.grupointegrado.bhpachulski.cadastrodeestado.model.Estado;

/**
 * Created by bhpachulski on 26/04/17.
 */

public class EstadoArrayAdapter extends ArrayAdapter<Estado> {

    private List<Estado> estados;

    public EstadoArrayAdapter(Context context, List<Estado> estados) {
        super(context, R.layout.row_estado, estados);

        this.estados = estados;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return super.getDropDownView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater =
                (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(R.layout.row_estado, null);

        TextView tvSigla = (TextView) view.findViewById(R.id.tvSigla);
        TextView tvNomeEstado = (TextView) view.findViewById(R.id.tvNomeEstado);

        Estado estado = estados.get(position);

        tvSigla.setText(estado.getSigla());
        tvNomeEstado.setText(estado.getNome());

        return view;
    }
}
