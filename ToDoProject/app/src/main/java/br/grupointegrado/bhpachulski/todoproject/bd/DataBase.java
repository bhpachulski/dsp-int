package br.grupointegrado.bhpachulski.todoproject.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.grupointegrado.bhpachulski.todoproject.R;
import br.grupointegrado.bhpachulski.todoproject.model.Categoria;
import br.grupointegrado.bhpachulski.todoproject.model.Midia;
import br.grupointegrado.bhpachulski.todoproject.model.MidiaType;
import br.grupointegrado.bhpachulski.todoproject.model.ToDo;

/**
 * Created by bhpachulski on 29/03/17.
 */

public class DataBase extends SQLiteOpenHelper {

    private Context context;

    private static final String DB_NAME = "toDoDataBase";
    private static final int DB_VERSION = 1;


    public DataBase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(
                context.getResources().getString(R.string.createTodo));

        sqLiteDatabase.execSQL(
                context.getResources().getString(R.string.createMidia));

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) { }

    public void insert (ToDo todo) {

        ContentValues cv = new ContentValues();
        cv.put("descricao", todo.getDescricao());
        cv.put("dataEntrega", todo.getDataEntrega());
        cv.put("prioridade", (int) todo.getPrioridade());
        cv.put("categoria", todo.getCategoria().getId());

        long idTodo = getWritableDatabase().insert("ToDo", null, cv);

        for (Midia m : todo.getMidias()) {
            ContentValues cvFoto = new ContentValues();

            cvFoto.put("idToDo", idTodo);
            cvFoto.put("nome", m.getNome());
            cvFoto.put("path", m.getLink());
            cvFoto.put("tipo", m.getTipo().toString());

            getWritableDatabase().insert("Midia", null, cvFoto);
        }

    }

    public List<ToDo> getTodos () {

        Cursor c = getReadableDatabase().rawQuery(
                context.getResources().getString(R.string.getTodos), null);

        List<ToDo> todos = new ArrayList<>();

        while (c.moveToNext()) {

            ToDo todo = new ToDo();
            todo.setId(c.getInt(c.getColumnIndex("id")));
            todo.setDescricao(c.getString(c.getColumnIndex("descricao")));
            todo.setEntrega(c.getString(c.getColumnIndex("dataEntrega")));
            todo.setPrioridade(c.getInt(c.getColumnIndex("prioridade")));
            todo.setCategoria(Categoria.getCategoria(c.getInt(c.getColumnIndex("categoria"))));

            todo.setMidias(this.getToDoMidia(todo.getId()));

            todos.add(todo);
        }

        return todos;
    }


    public ArrayList<Midia> getToDoMidia (Integer todoId) {

        Cursor c = getReadableDatabase().rawQuery(
                context.getResources().getString(R.string.getMidiaToDo), new String[]{todoId.toString()});

        ArrayList<Midia> midias = new ArrayList<>();

        while (c.moveToNext()) {
            Midia m = new Midia();
            m.setId(c.getInt(c.getColumnIndex("id")));
            m.setNome(c.getString(c.getColumnIndex("nome")));
            m.setLink(c.getString(c.getColumnIndex("path")));

            if (c.getString(c.getColumnIndex("tipo")).equals("FOTO"))
                m.setTipo(MidiaType.FOTO);
            else
                m.setTipo(MidiaType.AUDIO);

            midias.add(m);
        }

        return midias;
    }
}
