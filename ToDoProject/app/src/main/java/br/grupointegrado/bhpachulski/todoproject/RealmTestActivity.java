package br.grupointegrado.bhpachulski.todoproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import br.grupointegrado.bhpachulski.todoproject.realm.Aluno;
import br.grupointegrado.bhpachulski.todoproject.realm.Turma;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realm_test);


        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm realm = Realm.getInstance(config);

//        realm.beginTransaction();
//
//        Aluno a1 = new Aluno();
//        a1.setId(3);
//        a1.setNome("Bruno H.");
//
//        Aluno a2 = new Aluno();
//        a2.setId(4);
//        a2.setNome("Fabio P.");
//
//        Turma turma = realm.createObject(Turma.class, 4);
//        turma.setNome("DSP 2017/1");
//
//        turma.getAlunos().add(realm.copyToRealm(a1));
//        turma.getAlunos().add(realm.copyToRealm(a2));
//
//        realm.commitTransaction();

        for (Turma t : realm.where(Turma.class).findAll()) {
            Log.v("", t.getId() + " - " + t.getNome() + " [" + t.getAlunos().size());
        }
    }
}
