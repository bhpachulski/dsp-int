package br.grupointegrado.bhpachulski.todoproject.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by bhpachulski on 23/05/17.
 */

public class Turma extends RealmObject {

    @PrimaryKey
    private int id;

    @Required
    private String nome;

    private RealmList<Aluno> alunos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {

        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public RealmList<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(RealmList<Aluno> alunos) {
        this.alunos = alunos;
    }
}
