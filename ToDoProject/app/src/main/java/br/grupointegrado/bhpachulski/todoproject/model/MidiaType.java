package br.grupointegrado.bhpachulski.todoproject.model;

/**
 * Created by bhpachulski on 23/05/17.
 */

public enum MidiaType {

    FOTO, AUDIO;

}
