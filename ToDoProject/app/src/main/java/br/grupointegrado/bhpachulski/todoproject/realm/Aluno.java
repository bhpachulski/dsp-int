package br.grupointegrado.bhpachulski.todoproject.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bhpachulski on 23/05/17.
 */

public class Aluno extends RealmObject {

    @PrimaryKey
    private int id;

    private String nome;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
