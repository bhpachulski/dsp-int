package br.grupointegrado.bhpachulski.todoproject.model;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bhpachulski on 28/03/17.
 */

public class ToDo extends RealmObject implements Serializable {

    @PrimaryKey
    private int id;

    private String descricao;
    private Date entrega;
    private Integer prioridade;
    private Categoria categoria;

    private ArrayList<Midia> midias = new ArrayList<>();

    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDataEntrega () {
        return sdf.format(this.getEntrega());
    }

    public Date getEntrega() {
        return entrega;
    }

    public void setEntrega(String entrega) {
        try {
            this.entrega = sdf.parse(entrega);
        } catch (ParseException e) {}
    }

    public void setEntrega(Date entrega) {
        this.entrega = entrega;
    }

    public Integer getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(Integer prioridade) {
        this.prioridade = prioridade;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return this.getDescricao();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public ArrayList<Midia> getMidias() {
        return midias;
    }

    public void setMidia(Midia midia) {
        this.getMidias().add(midia);
    }


    public void setMidias(ArrayList<Midia> midias) {
        this.midias = midias;
    }
}
