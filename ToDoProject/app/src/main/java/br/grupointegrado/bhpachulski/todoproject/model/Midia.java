package br.grupointegrado.bhpachulski.todoproject.model;

import java.io.Serializable;

/**
 * Created by bhpachulski on 03/05/17.
 */

public class Midia implements Serializable {

    private int id;
    private String nome;
    private String link;
    private MidiaType tipo;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }




    @Override
    public String toString() {
        return this.getNome();
    }

    public MidiaType getTipo() {
        return tipo;
    }

    public void setTipo(MidiaType tipo) {
        this.tipo = tipo;
    }
}
