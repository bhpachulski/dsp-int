package br.grupointegrado.bhpachulski.todoproject;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import br.grupointegrado.bhpachulski.todoproject.bd.DataBase;
import br.grupointegrado.bhpachulski.todoproject.model.Categoria;
import br.grupointegrado.bhpachulski.todoproject.model.Midia;
import br.grupointegrado.bhpachulski.todoproject.model.MidiaType;
import br.grupointegrado.bhpachulski.todoproject.model.ToDo;

public class RegisterToDoActivity extends AppCompatActivity {

    private EditText edtDescricao;
    private EditText edtDataEntrega;
    private RatingBar rbPrioridade;
    private Spinner spnCategoria;

    static final int REQUEST_TAKE_PHOTO = 1;

    private ListView lvMidias;
    private ArrayList<Midia> midias = new ArrayList<>();
    private ArrayAdapter<Midia> aaMidias;

    private String midiaPath = "";

    private ToDo todoAlteracao;

    //audio
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;

    private static String mFileName = null;
    private MediaRecorder mRecorder = null;

    private boolean isRecording = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_to_do);

        try {

            todoAlteracao = (ToDo) getIntent().getSerializableExtra("todo");

            setToDo(todoAlteracao);

        } catch (NullPointerException e) {}

        edtDescricao = (EditText) findViewById(R.id.edtDescricao);
        edtDataEntrega = (EditText) findViewById(R.id.edtDataEntrega);
        rbPrioridade = (RatingBar) findViewById(R.id.rbPrioridade);

        List<Categoria> categoria = Arrays.asList(Categoria.values());

        spnCategoria = (Spinner) findViewById(R.id.spnCategoria);

        ArrayAdapter<Categoria> aaCategoria = new ArrayAdapter<Categoria>(this,
                android.R.layout.simple_list_item_1, categoria);

        spnCategoria.setAdapter(aaCategoria);

        lvMidias = (ListView) findViewById(R.id.lvMidias);

        aaMidias = new ArrayAdapter<Midia>(this, android.R.layout.simple_list_item_1, midias);

        lvMidias.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

                 getMenuInflater().inflate(R.menu.menu_midia, contextMenu);

            }
        });

        lvMidias.setAdapter(aaMidias);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo pListMenu = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Midia midiaSelecionada = midias.get(pListMenu.position);


        switch (item.getItemId()) {

            case R.id.mn_ver:

                    Intent i;

                    if (midiaSelecionada.getTipo() == MidiaType.AUDIO) {

                        i = new Intent();
                        i.setAction(android.content.Intent.ACTION_VIEW);
                        File file = new File(midiaSelecionada.getLink());
                        i.setDataAndType(Uri.fromFile(file), "audio/*");

                    } else {
                        i = new Intent(this, VerFoto.class);
                        i.putExtra("midia", midiaSelecionada);
                    }

                    startActivity(i);

                break;

            case R.id.mn_remover:

                midias.remove(pListMenu.position);
                aaMidias.notifyDataSetChanged();

                break;

        }

        return super.onContextItemSelected(item);
    }

    public void setToDo (ToDo todo) {

        edtDescricao.setText(todo.getDescricao());

        midias.addAll(todo.getMidias());
        aaMidias.notifyDataSetChanged();

    }

    public ToDo getToDo () {
        ToDo todo = new ToDo();

        todo.setDescricao(edtDescricao.getText().toString());
        todo.setEntrega(edtDataEntrega.getText().toString());
        todo.setPrioridade(rbPrioridade.getNumStars());

        todo.setCategoria((Categoria) spnCategoria.getSelectedItem());

        todo.setMidias(midias);

        return todo;
    }

    public void limpaActivity () {
        edtDescricao.setText("");
        edtDataEntrega.setText("");
        rbPrioridade.setRating(0);

        spnCategoria.setSelection(0);

        edtDescricao.requestFocus();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_padrao, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_salvar:

                //Usando o método
                ToDo todo = this.getToDo();

                DataBase db = new DataBase(this);
                db.insert(todo);

                this.limpaActivity();
                this.onBackPressed();

                Toast.makeText(this, "Salvar", Toast.LENGTH_SHORT).show();

                break;

            case R.id.action_cancelar:

                this.onBackPressed();

                Toast.makeText(this, "Cancelar", Toast.LENGTH_SHORT).show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void capturarFoto (View v) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Snackbar.make(v, "Erro ao capturar imagem.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

            if (photoFile != null) {

                Uri photoURI = FileProvider.getUriForFile(v.getContext(),
                        "br.grupointegrado.bhpachulski.todoproject.fileprovider",
                        photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {

            Midia foto = new Midia();
            foto.setNome("Foto " + String.valueOf(midias.size() + 1));
            foto.setLink(mCurrentPhotoPath);
            foto.setTipo(MidiaType.FOTO);

            midias.add(foto);

            aaMidias.notifyDataSetChanged();

            Toast.makeText(this, "Imagem capturada", Toast.LENGTH_LONG).show();
        }
    }

    String mCurrentPhotoPath;
    private File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private File createAudioFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String audioFileName = "AUDIO_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_MUSIC);
        File audio = File.createTempFile(
                audioFileName,
                ".3gp",
                storageDir
        );

        mFileName = audio.getAbsolutePath();
        return audio;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) finish();
    }

    private void startRecording() {

        try {
            createAudioFile();

            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setOutputFile(mFileName);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            try {
                mRecorder.prepare();
            } catch (IOException e) {
                Log.e("AudioRecording", "prepare() failed");
            }

            mRecorder.start();

            isRecording = true;
            Toast.makeText(this, "Gravação iniciada", Toast.LENGTH_LONG).show();
        } catch (IOException ioe) {
            Toast.makeText(this, "Não foi possível criar o arquivo de audio.", Toast.LENGTH_LONG).show();
        }
    }

    private void stopRecording() {
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;

        isRecording = false;
        Toast.makeText(this, "Gravação finalizada", Toast.LENGTH_LONG).show();

        addAudio ();
    }

    public void addAudio () {
        Midia m = new Midia();
        m.setLink(mFileName);
        m.setNome("Audio");
        m.setTipo(MidiaType.AUDIO);

        midias.add(m);
        aaMidias.notifyDataSetChanged();
    }

    public void btnRecording (View v) {

        if (isRecording)
            stopRecording();
        else
            startRecording();

    }
}
