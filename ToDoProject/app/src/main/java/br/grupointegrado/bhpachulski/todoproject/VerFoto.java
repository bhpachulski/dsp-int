package br.grupointegrado.bhpachulski.todoproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.io.File;

import br.grupointegrado.bhpachulski.todoproject.model.Midia;

public class VerFoto extends AppCompatActivity {

    private ImageView imgFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_foto);

        Intent intent = getIntent();
        Midia foto = (Midia) intent.getSerializableExtra("midia");

        File image = new File(foto.getLink());
        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath());

        imgFoto = (ImageView) findViewById(R.id.imgFoto);
        imgFoto.setImageBitmap(bitmap);

    }
}
